/*Alexandra Morrison
CSC-465
8 April 2019*/
/*Assignment 3*/

import java.util.*;
import java.util.concurrent.*;
import java.lang.*;

enum MeatType { LEAN, EXTRA_LEAN; }

class Dispatcher {

  BlockingQueue<MeatType> input_queue;
  Integer c_work_counter = 0;
  Integer c_extra_lean_counter = 0;
  Integer d_work_counter = 0;
  Integer d_extra_lean_counter = 0;

  Semaphore CPermissionProcessFirstItem = new Semaphore(1);
  Semaphore DPermissionProcessFirstItem = new Semaphore(1);

  Dispatcher(BlockingQueue<MeatType> iq) {
    System.out.println("Dispatcher constructor");
    input_queue = iq;
    try {
      CPermissionProcessFirstItem.acquire();
    } catch (InterruptedException ex) {
      System.out.println("Dispatcher Ugh 00");
    }
    try {
      DPermissionProcessFirstItem.acquire();
    } catch (InterruptedException ex) {
      System.out.println("Dispatcher Ugh 01");
    }
    FigureOutWhoIsEnabled();
  }

  public MeatType CGetBag() throws InterruptedException {
    try {
      CPermissionProcessFirstItem.acquire();
    } catch (InterruptedException ex) {
      System.out.println("CGetBag Ugh 00");
      throw ex;		// Somebody told us we're done.
    }
    MeatType mt = null;
    try {
      mt = input_queue.take();
    } catch (InterruptedException ex) {
      System.out.println("CGetBag Ugh 01");
      throw ex;		// Somebody told us we're done.
    }
    CPermissionProcessFirstItem.release();
    return (mt);
  }

  public MeatType DGetBag() throws InterruptedException {
    try {
      DPermissionProcessFirstItem.acquire();
    } catch (InterruptedException ex) {
      System.out.println("DGetBag Ugh 00");
      throw ex;		// Somebody told us we're done.
    }
    MeatType mt = null;
    try {
      mt = input_queue.take();
    } catch (InterruptedException ex) {
      System.out.println("DGetBag Ugh 01");
      throw ex;		// Somebody told us we're done.
    }
    DPermissionProcessFirstItem.release();
    return (mt);
  }

  private synchronized void FigureOutWhoIsEnabled() {
    /* Dispatch logic localized to here */
    MeatType mt_of_head_of_queue = input_queue.peek(); //  Handle null
    Integer c_surplus_work = c_work_counter - (4 * d_work_counter); // Avoid div by zero
    Integer c_surplus_extra_lean = c_extra_lean_counter - (3 * d_extra_lean_counter);
    if (c_surplus_work < 4) {	    // C can still process stuff
      if (c_surplus_extra_lean < 3) { // C can process either type of meat
        CPermissionProcessFirstItem.release();
      } else if (mt_of_head_of_queue == MeatType.EXTRA_LEAN) { // C cannot get further ahead
        DPermissionProcessFirstItem.release();
      } else {
        CPermissionProcessFirstItem.release();
      }
    } else {
      if ((c_surplus_extra_lean < 3) && (mt_of_head_of_queue == MeatType.EXTRA_LEAN)) { // Forbid D
        CPermissionProcessFirstItem.release();
      } else {
        DPermissionProcessFirstItem.release();
      }
    }
  }

  public synchronized void CProcessedABag(MeatType mt) {
    c_work_counter++;
    if (MeatType.EXTRA_LEAN == mt) {
      c_extra_lean_counter++;
    }
    // System.out.println("CProcessedABag " + mt + " " + c_extra_lean_counter + "/" + c_work_counter);
    FigureOutWhoIsEnabled();
  }

  public synchronized void DProcessedABag(MeatType mt) {
    d_work_counter++;
    if (MeatType.EXTRA_LEAN == mt) {
      d_extra_lean_counter++;
    }
    // System.out.println("DProcessedABag " + mt + " " + d_extra_lean_counter + "/" + d_work_counter);
    FigureOutWhoIsEnabled();
  }
}

/* I HAVE A question HERE: It seems that the trimmer throws 5 lbs of fat away from Machine A and then 10 lbs of fat from Machine B, kept it the way it was?*/
class Trimmer extends Thread {
  MeatType meat_type;
  BlockingQueue<Integer> input_queue;
  BlockingQueue<MeatType> output_queue;
  Integer hopper_amount = 0;
  Trimmer(String name, BlockingQueue<Integer> iq, BlockingQueue<MeatType> oq, MeatType mt) {
    super(name);
    System.out.println("Trimmer " + name + " constructor");
    meat_type = mt;
    input_queue = iq;
    output_queue = oq;
  }
  @Override
  public void run() {
    System.out.println("Trimmer start " + Thread.currentThread().getName());
    // If not enough for a new output bag, grab more.
    while (input_queue.size() > 0) {
      // Grab more if we need it
      if (hopper_amount < 15) {
        // System.out.println("Trimmer " + Thread.currentThread().getName() + " hopper amount 0 " + hopper_amount);
        try {
          // System.out.println("Trimmer " + Thread.currentThread().getName() + " getting more");
          hopper_amount += input_queue.take();
          // System.out.println("Trimmer " + Thread.currentThread().getName() + " hopper amount 1 " + hopper_amount);
        }
        catch (InterruptedException ex) {
          System.out.println("Trimmer Ugh 00");
        }
      }
      /* Random delay goes here */
      try {
        output_queue.put(meat_type);
        hopper_amount -= 15;
      } catch (InterruptedException ex) {
        System.out.println("Trimmer Ugh 01");
      }
      // System.out.println("Trimmer " + Thread.currentThread().getName() + " hopper amount 2 " + hopper_amount);
    }
  }
}

class Cutter extends Thread {
  Dispatcher dispatcher;
  BlockingQueue<MeatType> output_queue;
  Cutter(String name, Dispatcher d, BlockingQueue<MeatType> oq) {
    super (name);
    dispatcher = d;
    output_queue = oq;
    System.out.println("Cutter " + name + " constructor");
  }
  void random_delay() {
    // System.out.println("random_delay");
  }

}

class CutterC extends Cutter {
  CutterC(String name, Dispatcher d, BlockingQueue<MeatType> oq) {
    super(name, d, oq);
    System.out.println("CutterC " + name + " constructor");
  }
  @Override
  public void run() {
    Boolean more_input = true;
    while (more_input) {
      MeatType mt;
      try {
        mt = dispatcher.CGetBag();
        random_delay();
        output_queue.put(mt);
        dispatcher.CProcessedABag(mt);
      } catch (InterruptedException ex) {
        System.out.println("CutterC " + Thread.currentThread().getName() + " Ugh 00");
        more_input = false;
      }
    }
  }
}

class CutterD extends Cutter {
  CutterD(String name, Dispatcher d, BlockingQueue<MeatType> oq) {
    super(name, d, oq);
    System.out.println("CutterD " + name + " constructor");
  }
  @Override
  public void run() {
    Boolean more_input = true;
    while (more_input) {
      MeatType mt;
      try {
        mt = dispatcher.DGetBag();
        random_delay();
        output_queue.put(mt);
        dispatcher.DProcessedABag(mt);
      } catch (InterruptedException ex) {
        System.out.println("CutterD " + Thread.currentThread().getName() + " Ugh 00");
        more_input = false;
      }
    }
  }
}

class Packager extends Thread {
  BlockingQueue<MeatType> input_queue;
  Integer num_lean_packaged = 0;
  Integer num_extra_lean_packaged = 0;
  Packager(String name, BlockingQueue<MeatType> iq) {
    super(name);
    System.out.println("Packager " + name + " constructor");
    input_queue = iq;
  }
  public Integer numberLeanPackaged() {
    return num_lean_packaged;
  }
  public Integer numberExtraLeanPackaged() {
    return num_extra_lean_packaged;
  }
  @Override
  public void run() {
    Boolean more_input = true;
    while (more_input) {
      try {
        MeatType mt = input_queue.take();
        /* Random delay goes here */
        if (mt == MeatType.LEAN) {
          num_lean_packaged++;
        }
        else {
          num_extra_lean_packaged++;
        }
        /*
        System.out.println("Packager "
        + Thread.currentThread().getName()
        + " lean=" + num_lean_packaged
        + " extra=" + num_extra_lean_packaged); */
      } catch (InterruptedException ex) {
        System.out.println("Packager "  + Thread.currentThread().getName() + " Ugh 00");
        more_input = false;
      }
    }
    System.out.println("Packager "  + Thread.currentThread().getName() + " num_lean_packaged=" + num_lean_packaged);
    System.out.println("Packager "  + Thread.currentThread().getName() + " num_extra_lean_packaged=" + num_extra_lean_packaged);
  }
}

class Plant
{
  BlockingQueue<Integer>  Q1 = new LinkedBlockingDeque<Integer>(); // Weight explicit
  BlockingQueue<Integer>  Q2 = new LinkedBlockingDeque<Integer>(); // Weight explicit
  BlockingQueue<MeatType> Q3 = new LinkedBlockingDeque<MeatType>(); // Meat type explicit, weight implicitly 15
  BlockingQueue<MeatType> Q4 = new LinkedBlockingDeque<MeatType>();
  BlockingQueue<MeatType> Q5 = new LinkedBlockingDeque<MeatType>();

  Dispatcher dispatcher = new Dispatcher(Q3);

  Trimmer  A = new Trimmer("A", Q1, Q3, MeatType.LEAN);
  Trimmer  B = new Trimmer("B", Q2, Q3, MeatType.EXTRA_LEAN);

  //    Cutter   C = new Cutter("C", Q3, Q4);
  //    Cutter   D = new Cutter("D", Q3, Q5);
  CutterC C = new CutterC("C", dispatcher, Q4);
  CutterD D = new CutterD("D", dispatcher, Q5);

  Packager E = new Packager("E", Q4);
  Packager F = new Packager("F", Q5);

  int output[][] = { { 0, 0 }, { 0, 0 } };

  Plant(int[] input){
    System.out.println("Plant constructor");
    System.out.println("The input is: " + Arrays.toString(input));
    for (int q1idx = 0; q1idx < input[0]; q1idx++) {
      //	System.out.println("q1idx = " + q1idx);
      try {
        Q1.put(20);
      } catch (InterruptedException ex) {
        System.out.println("Plant Ugh 00");
      }
    }
    for (int q2idx = 0; q2idx < input[1]; q2idx++) {
      // System.out.println("q2idx = " + q2idx);
      try {
        Q2.put(25);
      } catch (InterruptedException ex) {
        System.out.println("Plant Ugh 02");
      }
    }
  }
  public void beginRunning() {
    A.start();
    B.start();
    C.start();
    D.start();
    E.start();
    F.start();
  }

  public void waitForThreadsFinish() {
    System.out.println("Starting joins");
    try {
      A.join();		// After this, A is done putting to Q3, and will exit.
      B.join();		// Likewise, B
      System.out.println("Q3 size: " + Q3.size());

      while (! Q3.isEmpty()) {
        Thread.currentThread().yield();
      }

      // Let Cutters know A & B are done, and all input is waiting on Q3
      C.interrupt();
      D.interrupt();
      System.out.println("Q3 size: " + Q3.size()); // Better be zero
      C.join();		// C is done and exited
      D.join();		// Likewise, D
      System.out.println("Q3 size: " + Q3.size()); // Likewise all zero
      System.out.println("Q4 size: " + Q4.size());
      System.out.println("Q5 size: " + Q5.size());

      // Let Packagers know C & D are done, and all input is on either Q4 or Q5
      E.interrupt();
      F.interrupt();

      // Packagers will exit when they've processed all their inputs.
      E.join();
      F.join();
    } catch (InterruptedException ex) {
      System.out.println("Plant Ugh 03");
    }
    System.out.println("Finished joins");
  }

  public int[][] tallyResults() {
    output[0][0] = E.numberLeanPackaged();
    output[0][1] = E.numberExtraLeanPackaged();
    output[1][0] = F.numberLeanPackaged();
    output[1][1] = F.numberExtraLeanPackaged();
    return(output);
  }
}


class Example_Program
{
  public static void main(String args[]){
    Grader obj = new Grader();
    int[] input = obj.input;                        // get random input array
    System.out.println("The input is: " + Arrays.toString(input));

    /***** PROGRAM HERE *****/

    //Renamed object here
    Plant myPlant = new Plant(input);
    // Call method beginRunning
    myPlant.beginRunning();
    myPlant.waitForThreadsFinish();

    int output[][] = myPlant.tallyResults();
    //        int [][] output = { { 22, 8 }, { 18, 2 } };     // correct ouput for [30, 20] input
    System.out.println("The output is: " + Arrays.deepToString(output));
    if (Grader.done(output)==0) {  // 0 is a normal exit
      System.exit(0);
    }
    else {
      System.exit(1);
    }
  }
}
